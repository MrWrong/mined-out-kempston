# mined-out-kempston

blog: <https://thefoggiest.dev/2024/05/30/mined-out-quicksilva-1983>

A binary patch that will let you use WASD keys or a Kempston joystick with Quicksilva's Mined Out (1983) for the Sinclair ZX Spectrum.

When applied to the original Mined Out from Quicksilva, the control keys 5 (left), 6 (down), 7 (up) and 8 (right) will be replaced with resp. A, S, W and D. The player will also be able to use their Kempston joystick to rescue Bill the Worm from certain old age and use the fire button to speed up the replay.

Install bspatch for your PC, copy the game as-is there, and apply the patch as follows:

`$ bspatch MINEDOUT.BAS MINEDKMP.BAS mined-out-kempston.patch`

Have fun!
